# DeviceManagement

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

##Bootstrap Installtion
1) Install bootstrap using npm

npm install bootstrap --save
after the installation of Bootstrap 4, we Need two More javascript Package that is Jquery and Popper.js without these two package bootstrap is not complete because Bootstrap 4 is using Jquery and popper.js package so we have to install as well

2) Install JQUERY

npm install jquery --save
3) Install Popper.js

npm install popper.js --save
Now Bootstrap is Install on you Project Directory inside node_modules Folder

open .angular-cli.json this file are available on you angular directory open that file and add the path of bootstrap, jquery, and popper.js files inside styles[] and scripts[] path see the below example

"styles": [   
    "../node_modules/bootstrap/dist/css/bootstrap.min.css",
    "styles.css"
  ],
  "scripts": [  
    "../node_modules/jquery/dist/jquery.min.js",
    "../node_modules/popper.js/dist/umd/popper.min.js",
    "../node_modules/bootstrap/dist/js/bootstrap.min.js"
  ],
Note: Don't change a sequence of js file it should be like this


*#*#*#**#*#*#**#*#*#**#*#*#* DB info *#*#*#**#*#*#**#*#*#**#*#*#*

Update device details api not available;-

1) Auth code details :

Request type:post

Body:
{
  "email": "harsh.z@tcs.com",
  "password": "admin"
}

Credentials:

User1: harsh.z@tcs.com / admin
User2: avinash@tcs.com / admin
User3: admin@tcs.com / admin

Api:
https://at5.herokuapp.com/auth/

Example:
Bearer authcode-token-code

************************************************************************************************************************************************************************************

2) All assets info;

API: https://at5.herokuapp.com/api/v1/assets
Hedare: send auth token.

Example:

[ { "asset_id": 1, "asset_name": "NA4034353406", "serial_no": "1212001359", "asset_model": "G5s plus", "color": "FFFFFF", "size": "5.65x2.79", "project_id": 1, "tagged_on": "2018-01-01T00:00:00.000Z", "assigned_on": "2018-01-01T00:00:00.000Z", "created_at": "2018-01-01T00:00:00.000Z", "updated_at": "2018-09-09T10:10:58.000Z", "asset_platform": { "asset_platform_id": 2, "name": "iOS", "image": "#N/A" }, "asset_type": { "asset_type_id": 7, "name": "phone", "image": "#N/A" }, "asset_manufacturer": { "asset_manufacturer_id": 5, "name": "motorola" }, "asset_state": { "asset_state_id": 1, "name": "Working" }, "asset_status": { "asset_status_id": 4, "name": "allocated" }, "tagged_to": { "user_id": 1, "email": "harsh.z@tcs.com", "employee_id": "1041737", "name": "Harsh Zalavadiya", "photo": "#N/A", "is_password_expired": 0, "is_locked": 0, "is_deleted": 0, "created_at": "2018-01-01T00:00:00.000Z", "updated_at": "2018-01-01T00:00:00.000Z", "role": { "role_id": 1, "name": "user", "description": "user" } }, "assigned_to": { "user_id": 1, "email": "harsh.z@tcs.com", "employee_id": "1041737", "name": "Harsh Zalavadiya", "photo": "#N/A", "is_password_expired": 0, "is_locked": 0, "is_deleted": 0, "created_at": "2018-01-01T00:00:00.000Z", "updated_at": "2018-01-01T00:00:00.000Z", "role": { "role_id": 1, "name": "user", "description": "user" } } }]


Suggestion:-
Os version not available.
asset_platform.version

************************************************************************************************************************************************************************************

3) To get particular asset details:

https://at5.herokuapp.com/api/v1/assets/1

1 is your asset id

************************************************************************************************************************************************************************************

4) Request new device

Request Type:PUT

API:  https://at5.herokuapp.com/api/v1/assets/request/4

Body:
{
 "serial_no": "4319998266"
}

4 is asset id

************************************************************************************************************************************************************************************

5) Return Device 

Request Type: PUT

API: https://at5.herokuapp.com/api/v1/assets/return/1


1 is your asset id

************************************************************************************************************************************************************************************
6) Reallocate Device:

Request Type: PUT	

API:https://at5.herokuapp.com/api/v1/assets/reallocate/4/2

Details: /assets/reallocate/{asset_id}/{user_id}

************************************************************************************************************************************************************************************

Link of DB GUI is http://dbadmin1.herokuapp.com/
DB Credentials
Username: raringringtail
Password: raringringtail



