import { Injectable } from '@angular/core';

export interface Data {
  userID: string;
}

@Injectable({
  providedIn: 'root'
})
export class DataShareService {

  userId: Data = { userID: "N/A",  }

  constructor() { }

  setUserID(id) {
    this.userId.userID = id;
    localStorage.setItem("userID", this.userId.userID);
  }
  getUserId() {
    this.userId.userID = localStorage.getItem("userID");
    return this.userId.userID;
  }
}
