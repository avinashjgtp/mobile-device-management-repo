import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APPURL } from '../../../core/appURL';


@Injectable({
  providedIn: 'root'
})
export class AnalyticsService {

  constructor(private httpClient: HttpClient) { }

  ngOnInit() {
  }

  getDeviceStatus(){
    return this.httpClient.get(APPURL.ADMIN_DEVICE_STATUS);
  }

  getDevicePlatforms(){
    return this.httpClient.get(APPURL.ADMIN_DEVICE_PLATFORM);
  }

  getDeviceStatusByPlatorms(){
    return this.httpClient.get(APPURL.ADMIN_DEVICE_STATUS_PLATFORM_WISE);
  }

}
