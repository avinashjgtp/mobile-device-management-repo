import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable, Subject } from 'rxjs'
import { APPURL } from '../core/appURL';


@Injectable({
  providedIn: 'root'
})
export class DeviceService {

  public deviceCountDetails = new Subject<any>();

  constructor(private httpClient: HttpClient) { }

  getDeviceList(): Observable<any> {
    return this.httpClient.get(APPURL.DEVICE_URL);
  }

  getDeviceDetailById() {


  }

  updateDeviceDetailById(id, allocationStatus) {
    console.log("Device id is" + id)
    let data = {
      "serial_no": allocationStatus
    }
    return this.httpClient.put(APPURL.REQUEST_DEVICE_URL + id, JSON.parse(JSON.stringify(data)));
  }

  deviceCount() {
    this.httpClient.get(APPURL.DEVICE_URL).subscribe((data: any[]) => {
      let detailJSON = {
        deviceTotal: data.length,
        deviceOccupy: 12,
        deviceAvailable: 50
      }
      this.deviceCountDetails.next(detailJSON);
    });
  }

  deviceReturn(id) {
    let url = APPURL.RETURN_DEVICE_URL + id;
    let body = {
      asset_id: id
    }
    return this.httpClient.put(url, body);
  }

  deviceReallocate(asset_Id, id) {
    let url = APPURL.DEVICE_REALLOCATE + asset_Id + "/" + id;
    let body = {
      asset_id: asset_Id,
      user_id: id
    }
    return this.httpClient.put(url, body);
  }

}
