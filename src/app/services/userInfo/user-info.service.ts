import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APPURL } from '../../core/appURL';
import { Subject } from "rxjs"

@Injectable({
  providedIn: 'root'
})
export class UserInfoService {
  public userData = new Subject<any>();
  public userId=new Subject<any>();

  constructor(private httpClient: HttpClient) {
  }

  getUserinfoByID(id) {
    this.httpClient.get(APPURL.USER_INFO_URL).subscribe(
      (data: any[]) => {
        console.log(data);
        for (let i = 0; i < data.length; i++) {
          if(data[i]._id === id )
          {
            console.log(data[i]);
            this.userData.next(data);
          }
          if(data[i].email === id )
          {
            console.log(id);
            let uId=data[i].user_id;
            console.log(uId);
            this.userId.next(uId);
          }

        }
        
      }
    );
  }

  getUserId(email){
    
  }
}
