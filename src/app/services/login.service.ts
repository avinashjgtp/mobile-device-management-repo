import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoggedInUser } from '../core/loggedin-user';
import { APPURL } from '../core/appURL';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  public loggedInUser: LoggedInUser;

  constructor(private httpClient: HttpClient) {
  }

  userLogin(userName, password) {
    let data = {
      email: userName,
      password: password
    }
    return this.httpClient.post(APPURL.AUTH_URL, data);
  }
  getUserDetails(id) {
    console.log("user Id:" + JSON.stringify(id));
  }
}
