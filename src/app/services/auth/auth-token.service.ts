
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpResponse, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthTokenService implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let token = localStorage.getItem('token');
    if (token == null) {
      token = "Bearer_auth_token";
    }
    let tokenizedReq = req.clone({
      headers: new HttpHeaders().append("Authorization", token)
    });
    return next.handle(tokenizedReq);
  }
}
