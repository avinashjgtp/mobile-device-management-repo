import { Injectable } from '@angular/core';
import { APPURL } from '../../core/appURL';
import { HttpClient } from '@angular/common/http'
import { Subject } from "rxjs"
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WorklogService {

  public userName = new Subject<any>();

  public localData: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);

  constructor(private http: HttpClient) {

  }

  getRequestOpen() {
    
    return this.http.get(APPURL.WORKLOG_REQUEST_OPEN);
  }


  getUserName(assetId, userId) {
    return this.http.get(APPURL.WORKLOG_REQUEST_OPEN).subscribe((userData: any[]) => {

      for (let i = 0; i < userData.length; i++) {

        if (userData[i].asset_id === assetId) {
          console.log(userData[i].created_by.name);
        }
      }
      this.userName.next(userData);
    });
  }
  updateDeviceStatus(status, w_id) {
    let url = APPURL.WORKLOG_REQUEST_STATUS + status + "/" + w_id;
    console.log(url);
    let body = {
      status: status,
      worklog_id: w_id
    }
    return this.http.put(url, body);
  }

}
