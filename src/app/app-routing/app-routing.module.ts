import { DashboardComponent } from "./../components/dashboard/dashboard.component";
import { DeviceManagementComponent } from "./../components/admin/device-management/device-management.component";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginComponent } from "../components/login/login.component";
import { DeviceItemComponent } from "../components/device-item/device-item.component";
import { DeviceListComponent } from "../components/device-list/device-list.component";
import { HamburgerMenuComponent } from "../components/hamburger-menu/hamburger-menu.component";
import { DeviceDetailComponent } from "../components/device-detail/device-detail.component";
import { DeviceRequestComponent } from "../components/device-request/device-request.component";
import { AdminDeviceRequestComponent } from "../components/admin/admin-device-request/admin-device-request.component";
import { AdminDashboardComponent } from "../components/admin/admin-dashboard/admin-dashboard.component";
import { NewRequestComponent } from '../components/new-request/new-request.component';
import { AdminChartsComponent } from "../components/admin/admin-charts/admin-charts.component";

const appRoutes: Routes = [
  {
    path: "dashboard",
    component: DashboardComponent,
    children: [
      { path: 'device-list', component: DeviceListComponent },
      { path: "device-detail/:id", component: DeviceDetailComponent },
      { path: "device-request", component: DeviceRequestComponent },
      // { path: "new-request", component: NewRequestComponent },
      { path: "", redirectTo: 'device-list', pathMatch: "full" },
    ]
  },
  {
    path: "admin-dashboard", component: AdminDashboardComponent,
    children: [
      { path: 'device-list', component: DeviceListComponent },
      { path: "device-detail/:id", component: DeviceDetailComponent },
      { path: "admin-device-request", component: AdminChartsComponent },
      { path: "device-request", component: DeviceRequestComponent },
      { path: "", redirectTo: 'admin-device-request', pathMatch: "full" },

    ]
  },

  { path: "login", component: LoginComponent },
  { path: '**', redirectTo: 'login' }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
