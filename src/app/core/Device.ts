export class Device{
    private _assetId: string;
    private _title: string;
    private _description: string;

    constructor(assetId, title, description){
        this._assetId = assetId;
        this._title = title;
        this._description = description;
    }

    get assetId(){
        return this._assetId;
    }
    set assetId(assetId){
        this._assetId = assetId;
    }

    get title(){
        return this._title;
    }
    set title(title){
        this._title = title;
    }

    get description(){
        return this._description;
    }    
    set description(description){
         this._description = description;
    }

        // _id: string;
        // assetId: string;
        // type: string;
        // model: string;
        // status: string;
        // projectId: string;
        // taggedTo: string;
        // isWith: string;
        // taggedOn: string;
        // assignedOn: string;
        // __v: string;

}