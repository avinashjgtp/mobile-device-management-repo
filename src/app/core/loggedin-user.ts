export class LoggedInUser {
  private _userName: string;

  private _password: string;

  private _authToken: string;

  private _userDetails: Object;

  public _userId: string;

  constructor(userName, password) {
    this._userName = userName;
    this._password = password;

  }
  get userId() {
    return this._userId;
  }
  set userID(userId) {
    this._userId = userId;
  }

  get userName() {
    return this._userName;
  }

  get password() {
    return this._password;
  }

  get authToken() {
    return this._authToken;
  }

  get userDetails() {
    return this._userDetails;
  }

  set userName(username) {
    this._userName = username;
  }

  set password(password) {
    this._password = password;
  }

  set authToken(authToken) {
    this._authToken = authToken;
  }

  set userDetails(userDetails) {
    this._userDetails = userDetails;
  }
}
