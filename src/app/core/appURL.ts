export class APPURL {
    public static AUTH_URL = "https://at5.herokuapp.com/auth/";
    public static DEVICE_URL="https://at5.herokuapp.com/api/v1/assets";
    public static REQUEST_DEVICE_URL="https://at5.herokuapp.com/api/v1/assets/request/"
    public static USER_INFO_URL="https://at5.herokuapp.com/api/v1/users";
    public static WORKLOG_REQUEST_OPEN="https://at5.herokuapp.com/api/v1/worklog/requests/open";
    public static WORKLOG_REQUEST_STATUS="https://at5.herokuapp.com/api/v1/worklog/requests/";
    public static RETURN_DEVICE_URL="https://at5.herokuapp.com/api/v1/assets/return/";
    public static DEVICE_REALLOCATE="https://at5.herokuapp.com/api/v1/assets/reallocate/";

    public static ADMIN_DEVICE_STATUS="https://at5.herokuapp.com/api/v1/analytics/device_status";
    public static ADMIN_DEVICE_PLATFORM="https://at5.herokuapp.com/api/v1/analytics/device_platforms";
    public static ADMIN_DEVICE_STATUS_PLATFORM_WISE="https://at5.herokuapp.com/api/v1/analytics/device_status/platform_wise";
}