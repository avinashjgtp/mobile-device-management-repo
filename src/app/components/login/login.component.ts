import { Component, OnInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../../services/login.service';
import { data } from '../../core/data';
import { DataShareService } from "../../services/dataSharing/data-share.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  isParaActive: Boolean = false;
  isFirstLogin: Boolean = false;
  showLoader = false;

  emailId: string;
  password: string;

  loginForm: FormGroup;
  user: any = {};
  resetForm: FormGroup;


  // public loggedInUser: LoggedInUser;

  loginData: data = {
    username: '',
    password: ''
  }

  usernameAlert: String = 'Please fill username';
  passwordAlert: String = 'Please fill password';
  loginAlert: string;
  loginError: Boolean = false;

  constructor(fb: FormBuilder,
    private loginService: LoginService,
    private dataShare: DataShareService,
    private router: Router) {
      
    // login form
    this.loginForm = fb.group({
      username: [null, Validators.required],
      password: [null, Validators.required]
    });

    // reset form
    this.resetForm = fb.group({
      oldPassword: [null, Validators.required],
      newPassword: [null, Validators.required],
      confirmPassword: [null, Validators.required]
    });
  }


  navigateToResetForm() {
    this.isFirstLogin = true;
  }

  navigateToDashboard() {
    this.router.navigateByUrl('/dashboard');
  }

  showError(errorMessage) {
    console.log(errorMessage);
  }

  submitResetForm(value) {
    console.log('Resetting the password', value);
    if (this.isPasswordValid(value)) {
      this.navigateToDashboard();
    } else {
      this.showError('Password Reset Failed');
    }
  }

  isPasswordValid(value) {
    const isOldPasswordSame = this.loginService.loggedInUser.password === value.oldPassword;
    const isNewPasswordSame = value.newPassword === value.confirmPassword;
    return isOldPasswordSame && isNewPasswordSame;
  }

  // forgot password
  resetPassword() {
    this.isParaActive = !this.isParaActive;
  }

  login() {
    this.showLoader = true;
    this.loginService.userLogin(this.loginForm.get("username").value, this.loginForm.get("password").value).subscribe((data) => {
      console.log(data["role"].name);

      let role = data["role"].name;
      let token = data["token"];
      let userId = data["user_id"];
      let value = localStorage.setItem("token", 'Bearer '+token);
      console.log("User id is:"+userId)
      
      if (userId) {
        this.dataShare.setUserID(userId); //Set user id
        if (role === 'user') {
          this.router.navigateByUrl('/dashboard');
        }
        else if (role === 'admin') {
          this.router.navigateByUrl('/admin-dashboard');
        }
        else {
          console.log("Please contact admin")
        }
      }

    },
      (err) => {
        this.showLoader = false;
        console.log("error:" + JSON.stringify(err));
      }
    );

  }
}

