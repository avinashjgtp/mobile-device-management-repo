import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDeviceRequestComponent } from './admin-device-request.component';

describe('AdminDeviceRequestComponent', () => {
  let component: AdminDeviceRequestComponent;
  let fixture: ComponentFixture<AdminDeviceRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDeviceRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDeviceRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
