import { Component, OnInit } from '@angular/core';
import { DeviceService } from '../../../services/device.service';
import { LoginService } from "../../../services/login.service";
import { Router } from '@angular/router';
import { UserInfoService } from "../../../services/userInfo/user-info.service";
import { Subscription } from "rxjs"


@Component({
  selector: 'app-admin-device-request',
  templateUrl: './admin-device-request.component.html',
  styleUrls: ['./admin-device-request.component.css']
})
export class AdminDeviceRequestComponent implements OnInit {

  requestedDevice = [];
  subscription: Subscription;


  constructor(private deviceService: DeviceService,
    private router: Router,
    private userInfo: LoginService,
    private userDetails: UserInfoService
  ) { }

  ngOnInit() {
    this.deviceService.getDeviceList().subscribe(
      (data: any[]) => {
        for (let i = 0; i < data.length; i++) {
          if (data[i].allocationStatus === "requested") {

            this.userDetails.getUserinfoByID(data[i].assignedTo);
            this.subscription = this.userDetails.userData.subscribe(
              (data: any[]) => {
                for (let i = 0; i < data.length; i++) {
                 // console.log(data[i].firstName);
                  this.requestedDevice[i].assignedTo = data[i].firstName;
                }
              }
            );
            this.requestedDevice.push(data[i]);
          }
        }
      }
    );
  }
  accept(id) {
    let allocationStatus = "allocated";
    this.deviceService.updateDeviceDetailById(id, allocationStatus).subscribe(
      (data) => {
        if (data) {
          alert("Request Approved")
        }
      }
    );
  }

  cancel(id) {
    let allocationStatus = "available";
    this.deviceService.updateDeviceDetailById(id, allocationStatus).subscribe(
      (data) => {
        if (data) {
          alert("Request Cancel")

        }
      }
    );
  }


}
