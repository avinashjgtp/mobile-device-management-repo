import { Component, OnInit } from '@angular/core';
import { userRole } from '../../../core/role';
import { Event, Router, ActivatedRoute, NavigationStart, NavigationEnd } from '@angular/router'


@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {

  showLoadingIndicator = true;

  constructor(private _router: Router,
    private _activatedRoute: ActivatedRoute) {
    userRole.isAdmin = true;
    this._router.events.subscribe(((routerEvent: Event) => {
      if (routerEvent instanceof NavigationStart) {
        this.showLoadingIndicator = true;
        console.log("loader status:" + this.showLoadingIndicator);
      }
      if (routerEvent instanceof NavigationEnd) {
        this.showLoadingIndicator = false;
        console.log("loader status:" + this.showLoadingIndicator);
      }

    }));

  }

  ngOnInit() {

  }

}
