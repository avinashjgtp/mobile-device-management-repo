import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import { AnalyticsService } from "../../../services/admin/analytics/analytics.service";
import { Chart } from "chart.js";

@Component({
  selector: "app-admin-charts",
  templateUrl: "./admin-charts.component.html",
  styleUrls: ["./admin-charts.component.css"]
})
export class AdminChartsComponent {
  @ViewChild("deviceStatusChart")
  deviceStatusChart: ElementRef;

  @ViewChild("devicePlatformChart")
  devicePlatformChart: ElementRef;

  @ViewChild("deviceStatusAndroid")
  deviceStatusAndroid: ElementRef;

  @ViewChild("deviceStatusIos")
  deviceStatusIos: ElementRef;

  @ViewChild("deviceStatusOther")
  deviceStatusOther: ElementRef;


  chart;

  constructor(private analyticsService: AnalyticsService) {}

  ngAfterViewInit() {
    this.renderDeviceStatusChart();
    this.renderDevicePlatformChart();
    this.renderDeviceStatusChartPlatformWise();
  }

  renderDeviceStatusChart() {
    const context = this.deviceStatusChart.nativeElement.getContext("2d");
    this.analyticsService.getDeviceStatus().subscribe(d => {
      this.createDoughnutChart(context, d);
    });
  }

  renderDevicePlatformChart() {
    const context = this.devicePlatformChart.nativeElement.getContext("2d");
    this.analyticsService.getDevicePlatforms().subscribe(d => {
      this.createDoughnutChart(context, d);
    });
  }

  renderDeviceStatusChartPlatformWise() {
    this.analyticsService.getDeviceStatusByPlatorms().subscribe(d => {
      const contextAndroid = this.deviceStatusAndroid.nativeElement.getContext("2d");
      const contextIos = this.deviceStatusIos.nativeElement.getContext("2d");
      const contextOther = this.deviceStatusOther.nativeElement.getContext("2d");
      this.createDoughnutChart(contextAndroid, d['Android']);
      this.createDoughnutChart(contextIos, d['iOS']);
      this.createDoughnutChart(contextOther, d['Other']);
    });
  }

  createDoughnutChart(context, data) {
    this.chart = new Chart(context, {
      type: "doughnut",
      data: {
        datasets: [
          {
            data: data.count,
            backgroundColor: data.colors
          }
        ],
        labels: data.labels
      },
      options: {
        responsive: true,
        legend: {
          position: "bottom"
        },
        title: {
          display: true,
          text: `${data.title} - total ${data.total}`
        },
        animation: {
          animateScale: true,
          animateRotate: true
        }
      }
    });
  }
}
