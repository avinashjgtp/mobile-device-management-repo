import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {trigger, state, style, animate, transition, keyframes} from '@angular/animations';
import { Router } from '@angular/router';
import { routerNgProbeToken } from '@angular/router/src/router_module';

@Component({
  selector: 'app-hamburger-menu',
  templateUrl: './hamburger-menu.component.html',
  styleUrls: ['./hamburger-menu.component.css']
})
export class HamburgerMenuComponent implements OnInit {
  @ViewChild('mySidenav') element: ElementRef;

  constructor() {
  }

  ngOnInit() {
  }

  // ngAfterViewInit() {
  //   this.element.nativeElement.style.width = '0px';
  // }

  openNav() {
    this.element.nativeElement.style.width = '250px';
  }

  closeNav() {
    this.element.nativeElement.style.width = '0px';
  }

}
