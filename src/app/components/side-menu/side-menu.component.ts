import { Component, OnInit } from '@angular/core';
import { userRole  } from '../../core/role';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.css']
})
export class SideMenuComponent implements OnInit {

  isParaActive: Boolean = false;
  isAdmin:Boolean=userRole.isAdmin;
  
  constructor() { }

  ngOnInit() {
  }


  toggleClass() {
    this.isParaActive = !this.isParaActive;
  }
}
