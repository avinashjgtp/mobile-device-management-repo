import { Component, OnInit } from '@angular/core';
import { DeviceService } from "../../services/device.service";
import { DataShareService } from '../../services/dataSharing/data-share.service';
import { UserInfoService } from '../../services/userInfo/user-info.service';
import { Subscription } from "rxjs"
import { Router } from '@angular/router'
import { WorklogService } from '../../services/worklog/worklog.service';

@Component({
  selector: 'app-device-request',
  templateUrl: './device-request.component.html',
  styleUrls: ['./device-request.component.css']
})
export class DeviceRequestComponent implements OnInit {

  private getUserID: Subscription;

  userID = this.dataShare.getUserId();
  accupay = [];
  requested = [];
  showLoader = true;
  emailSer = ""
  disableStatus = false;

  constructor(private deviceService: DeviceService,
    private route: Router,
    private worklog: WorklogService,
    private userInfo: UserInfoService,
    private dataShare: DataShareService) { }

  ngOnInit() {
    this.deviceService.getDeviceList().subscribe(
      (data: any[]) => {
        for (let i = 0; i < data.length; i++) {
          if (data[i].assigned_to.user_id === Number(this.userID)) {
            this.showLoader = false;

            if (data[i].asset_status.name === "allocated") {
              this.accupay.push(data[i]);
            }
          }
        }
      }
    );

    this.worklog.getRequestOpen().subscribe((openWorklog: any[]) => {
      for (let i = 0; i < openWorklog.length; i++) {
     

        if(openWorklog[i].request_type === "asset-reallocate" && openWorklog[i].asset_to.user_id === Number(this.userID))
        {
            this.requested.push(openWorklog[i]);
            this.showLoader = false;
        }

        if (openWorklog[i].asset.tagged_to.user_id === Number(this.userID) && openWorklog[i].log_type === "request" || openWorklog[i].asset_status.name === "return-requested" && openWorklog[i].asset.tagged_to.user_id ) {
          this.requested.push(openWorklog[i]);
          console.log(openWorklog[i].asset.asset_model);
          this.showLoader = false;
        }
        

      }
    })
  }

  decision(w_id, dec) {
    let type = dec;
    if (type === "accpet") {
      let allocationStatus = "accept";
      this.worklog.updateDeviceStatus(allocationStatus, w_id).subscribe((data) => {
        console.log(data);
        alert("request Accepted")
        if (data) {
          console.log("Success Data:" + data);
          window.location.reload()
        }
      })
      console.log(w_id, dec);
    }
    else if (type === "reject") {
      let allocationStatus = "reject";
      this.worklog.updateDeviceStatus(allocationStatus, w_id).subscribe((data) => {
        console.log(data)
        this.disableStatus = true;
        window.location.reload()
        alert("request rejected")
      })
    }
    else {
      console.log("somthing wrong with decision")
    }
  }

  onKeyUp(event: any) {
    this.emailSer = event.target.value;
  }

  transfer(asset_Id) {
    this.userInfo.getUserinfoByID(this.emailSer);
    this.getUserID = this.userInfo.userId.subscribe((id) => {
      this.deviceService.deviceReallocate(asset_Id, id).subscribe((data: any) => {
        console.log("Reallocation Status: " + data.statusCode);
        if (data.statusCode === 200) {
          alert("Device reallocation request send.")
          window.location.reload();
        }
        else {
          alert("Error: Please check emailƒ id.")
        }
      });
    });
  }


  returnDevice(id) {
    console.log(id);
    this.deviceService.deviceReturn(id).subscribe((data: any) => {
      let status = data.statusCode;
      console.log(status)
      if (status === 200) {

        alert("Device request sent successfully.")
        window.location.reload()
      }
    });
  }

}
