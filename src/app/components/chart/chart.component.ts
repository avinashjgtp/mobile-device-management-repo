import { Component, OnInit } from '@angular/core';
import { Chart } from "chart.js";
import { DeviceService } from "../../services/device.service"
import { Subscription } from "rxjs"

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {

  chart = [];
  private subcription: Subscription;  
   deviceChartDetails = {
    total: ""
  }

  

  constructor(private deviceCount: DeviceService) { }

  ngOnInit() {
    this.deviceCount.deviceCount();

    this.subcription = this.deviceCount.deviceCountDetails.subscribe((data) => {
      
      this.deviceChartDetails.total = data.deviceTotal;
      console.log(this.deviceChartDetails);
    })

    console.log("Details from count service:" + JSON.stringify(this.deviceChartDetails));
    this.chart = new Chart("DeviceChart", {
      type: 'doughnut',
      data: {
        labels: ["Occupy", "Available"],
        datasets: [
          {
            label: "Device Status ",
            backgroundColor: ["rgba(46, 204, 113, 1)", "rgba(39, 174, 96, 1)"],
            data: [27, 123]
          }
        ]
      },
      options: {
        title: {
          display: true,
          showTooltips: true,

          text: "Device Status (Total -" + this.deviceChartDetails.total + ")"
        }
      }
    });
  }

}
