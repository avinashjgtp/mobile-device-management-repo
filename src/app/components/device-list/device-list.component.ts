import { Component, OnInit } from '@angular/core';
import { DeviceService } from '../../services/device.service';


@Component({
  selector: 'app-device-list',
  templateUrl: './device-list.component.html',
  styleUrls: ['./device-list.component.css']
})
export class DeviceListComponent implements OnInit {
  devices: any[];
  iphoneDevice = [];
  ipadDevice = [];
  androidDevice = [];
  androidTablet = [];
  windowPhone = [];
  other = [];
  
  tap: string = "all";
  constructor(private deviceService: DeviceService) {
    
   
    this.deviceService.getDeviceList().subscribe(
      (data: any[]) => {
        if (data.length) {
          this.devices=data;

          console.log("Array "+this.devices[0].asset_type.name);
          for (let i = 0; i < this.devices.length; i++) {
            console.log(this.devices[i]);
            if (this.devices[i].asset_type.name == "iphone") {
              //console.log(this.devices[i].asset_id)
              this.iphoneDevice.push(this.devices[i]);
            }
            if (this.devices[i].asset_type.name == "ipad") {
              this.ipadDevice.push(this.devices[i]);
            }
            if (this.devices[i].asset_type.name == "phone") {
              this.androidDevice.push(this.devices[i]);
            }
            if (this.devices[i].asset_type.name == "androidTablet") {
              this.androidTablet.push(this.devices[i]);
            }
            if (this.devices[i].asset_type.name == "windows") {
              this.windowPhone.push(this.devices[i]);
            }
            if (this.devices[i].asset_type.name == "watch") {
              this.other.push(this.devices[i]);
            }
          }
        }
      }
    );
    // console.log(this.iphoneDevice);

  }
  ngOnInit() {
  }

}
