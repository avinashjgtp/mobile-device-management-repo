import { Component, OnInit } from '@angular/core';
import { DeviceService } from '../../services/device.service';
import { LoginService } from "../../services/login.service";
import { Router } from '@angular/router';
import { UserInfoService } from "../../services/userInfo/user-info.service";
import { Subscription } from "rxjs"
import { DataShareService } from '../../services/dataSharing/data-share.service';
import { data } from "../../core/data";
import { WorklogService } from '../../services/worklog/worklog.service';

@Component({
  selector: 'app-new-request',
  templateUrl: './new-request.component.html',
  styleUrls: ['./new-request.component.css']
})
export class NewRequestComponent implements OnInit {

  requestedDevice = [];
  subscription: Subscription;
  userID: string;

  constructor(
    private deviceService: DeviceService,
    private router: Router,
    private userInfo: LoginService,
    private dataShare: DataShareService,
    private userDetails: UserInfoService,
    private worklog: WorklogService
  ) {
    console.log("length:" + this.dataShare.getUserId());
    this.userID = this.dataShare.getUserId();
  }

  ngOnInit() {

    this.worklog.getRequestOpen().subscribe((openWorklog: any[]) => {

      for (let i = 0; i < openWorklog.length; i++) {
       
        if (openWorklog[i].asset.tagged_to.user_id === Number(this.userID) && openWorklog[i].log_type === "request" || openWorklog[i].asset_status.name === "return-requested") {
          this.requestedDevice.push(openWorklog[i]);
        }

      }
    })
    
  }
  accept(id) {
    let allocationStatus = "accept";
    this.worklog.updateDeviceStatus(allocationStatus,id).subscribe((data)=>{
      console.log(data);
      alert("request Accepted")
      if(data){
        this.router.navigateByUrl("/dashboard/new-request");
        //this.ngOnInit();
      }
    })
  }

  cancel(id) {
    let allocationStatus = "reject";
    this.worklog.updateDeviceStatus(allocationStatus,id).subscribe((data)=>{
      console.log(data)
      alert("request rejected")
    })
  }

}
