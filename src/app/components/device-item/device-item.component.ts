import { Component, OnInit, Input } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { userRole } from '../../core/role';

@Component({
  selector: 'app-device-item',
  templateUrl: './device-item.component.html',
  styleUrls: ['./device-item.component.css']
})
export class DeviceItemComponent implements OnInit {

  // @Input() deviceImage: string;
  @Input() deviceAssetId: string;
  @Input() deviceTitle: string;
  @Input() deviceDescription: string;
  @Input() allocationStatus: string;

  constructor(private router: Router,
    private activatedRouter: ActivatedRoute) {


    if (this.deviceTitle == null) {
      this.deviceTitle = "not  Avaliable";
    }
  }

  ngOnInit() {
  }

  viewDeviceDetail() {
    //this.router.navigate(['/device-detail', this.deviceAssetId]);

  }

  btnRequest(event) {
    if (userRole.isAdmin) {
      this.router.navigate(['/admin-dashboard/device-detail', this.deviceAssetId]);
    }
    else {
      this.router.navigate(['/dashboard/device-detail', this.deviceAssetId]);
    }


  }


}
