import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DeviceService } from '../../services/device.service';
import { userRole } from '../../core/role';

@Component({
  selector: 'app-device-detail',
  templateUrl: './device-detail.component.html',
  styleUrls: ['./device-detail.component.css']
})
export class DeviceDetailComponent implements OnInit {
  private assetId: string;
  allDevice = [];
  deviceDetails = {
    "A_id": "Asset ID",
    "asset_value": "N/A",
    "D_name": "Device_name",
    "D_value": "N/A",
    "S_no": "Serial No",
    "S_value": "N/A",
    "OS": "OS",
    "OS_value": "N/A",
    "OS_version": "OS Version",
    "OS_version_value": "N/A",
    "Device_size": "Device Size",
    "Device_size_value": "N/A",
    "Device_color": "Device Color",
    "Device_color_value": "N/A",
    "Device_id": ""
  }
  constructor(private route: ActivatedRoute,
    private router: Router,
    private deviceDetialsService: DeviceService) {

    this.route.params.subscribe(params => this.assetId = params.id);
    deviceDetialsService.getDeviceList().subscribe(
      (data: any[]) => {
        this.allDevice = data;
        for (let i = 0; i < this.allDevice.length; i++) {
          if (this.allDevice[i].asset_id == this.assetId) {
            this.deviceDetails.asset_value = this.allDevice[i].asset_id;
            this.deviceDetails.Device_id = this.allDevice[i].asset_name;
            this.deviceDetails.D_value = this.allDevice[i].asset_model;
            this.deviceDetails.Device_color_value = this.allDevice[i].color;
            this.deviceDetails.Device_size_value = this.allDevice[i].size;
            this.deviceDetails.OS_value = this.allDevice[i].asset_platform.name;
            this.deviceDetails.OS_version_value = this.allDevice[i].asset_platform.version;
            this.deviceDetails.S_value = this.allDevice[i].serial_no;
          }
        }
      });
  }

  ngOnInit() {

  }

  request(id, s_value) {
    let allocationStatus = s_value;
    console.log("Reqiest Id:" + id + "S_no:" + allocationStatus);
    this.deviceDetialsService.updateDeviceDetailById(id, allocationStatus).subscribe((data:any) =>{
      console.log(data.statusCode);
      if(data.statusCode === 200){
        this.router.navigateByUrl('/dashboard');
      }
    }
     );
    // .subscribe(
    //   (data) => {
    //     alert("Request sent to admin:"+data);
    //     if (data) {
    //      // alert("Request sent to admin ");
    //       if (userRole.isAdmin) {
    //         this.router.navigateByUrl('/admin-dashboard/device-list');
    //       }
    //       else {
    //         this.router.navigateByUrl('/dashboard');
    //       }

    //     }
    //   },
    //   (error) => {
    //     console.log(error);
    //   }
    // );
  }

}
