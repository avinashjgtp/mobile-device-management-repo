import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatToolbarModule, MatSidenavModule, MatSidenavContent, MatSidenavContainer } from '@angular/material';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { DeviceListComponent } from './components/device-list/device-list.component';
import { DeviceItemComponent } from './components/device-item/device-item.component';

import { AppRoutingModule } from './app-routing/app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HamburgerMenuComponent } from './components/hamburger-menu/hamburger-menu.component';
import { DeviceDetailComponent } from './components/device-detail/device-detail.component';
import { HeaderBarComponent } from './components/header-bar/header-bar.component';
import { SideMenuComponent } from './components/side-menu/side-menu.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DeviceManagementComponent } from './components/admin/device-management/device-management.component';
import { DeviceRequestComponent } from './components/device-request/device-request.component';


import { LoginService } from './services/login.service';
import { PageLoaderComponent } from './components/page-loader/page-loader.component';// Login Service
import { AuthTokenService } from './services/auth/auth-token.service';// Auth token saver in header
import { AdminDeviceRequestComponent } from './components/admin/admin-device-request/admin-device-request.component';
import { AdminDashboardComponent } from "./components/admin/admin-dashboard/admin-dashboard.component";
import { ChartComponent } from './components/chart/chart.component';
import { NewRequestComponent } from './components/new-request/new-request.component';
import { WorklogService } from './services/worklog/worklog.service'; //Log service
import { AdminChartsComponent } from './components/admin/admin-charts/admin-charts.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DeviceListComponent,
    DeviceItemComponent,
    HamburgerMenuComponent,
    DeviceDetailComponent,
    HeaderBarComponent,
    SideMenuComponent,
    DashboardComponent,
    DeviceManagementComponent,
    DeviceRequestComponent,
    AdminDeviceRequestComponent,
    AdminDashboardComponent,
    PageLoaderComponent,
    ChartComponent,
    NewRequestComponent,
    AdminChartsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatToolbarModule,
    MatSidenavModule,
    NgbModule.forRoot(),
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [LoginService, {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthTokenService,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
